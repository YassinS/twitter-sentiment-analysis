# Twitter-Sentiment-Analysis

Gets tweets from Twitter, containing user-given keywords and performs a sentiment analysis on the textbodies. 
The polarity, subjectivity and time values are getting saved in an SQLite3 database.

## Example Usage

`python twitter-analysis.py KEYWORD1 KEYWORD2 KEYWORDN`

  1. Gets all tweets containing KEYWORD1, KEYWORD2 and KEYWORDN.
  2. Analyzes the polarity and subjectivity and also gets the currrent time.
  3. Saves polarity,subjectivity and time, in the data.db SQLite3 Database.

## Requirements
  1. Python 3.x.x
     `https://www.python.org/downloads/`
  2. Tweepy
     `pip install -U tweepy`
  3. TextBlob
     `pip install -U textblob`

## Coming Features
  In the next couple of days I will add a second script, that plots the data
  and analyzes it with a linear regression analysis.
